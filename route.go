package main

import (
	"github.com/golang/geo/r3"
)	

type RoutePoint struct {
	sat *Satellite
	Route []*Satellite
	expense float64
	expenseTo float64
}

func runAStar(stack *PriorityQueue, end r3.Vector) []*Satellite {
	rp := stack.Pop()
	for rp != nil {
		rp.sat.Checked = true
		//fmt.Printf("Checking %v\n", rp.sat.Name)
		for _,sat := range rp.sat.VisibleSatellites {
			if sat.Checked {
				continue
			}
			if sat.EndPoint {
				//fmt.Printf("FOUND FINAL RELAY: %v\n", sat.Name)
				return append(rp.Route, sat)
			}
			dist := end.Sub(sat.Pos).Norm()
			newRp := &RoutePoint{sat, append(rp.Route, sat), rp.expenseTo + R + dist, rp.expenseTo + R}
			stack.Push(newRp)
		}
		rp = stack.Pop()
	}
	return nil
}

func findRoute(c *Constellation, start, end r3.Vector) []*Satellite {
	var stack PriorityQueue 
	
	for index,sat := range c.Satellites {
		if hasLineOfSight(end, sat.Pos) {
			c.Satellites[index].EndPoint = true
		} else {
			c.Satellites[index].EndPoint = false
		}			
	}
	
	for index := range c.Satellites {
		sat := &c.Satellites[index]
		if hasLineOfSight(start, sat.Pos) {
			var t []*Satellite
			dist := end.Sub(sat.Pos).Norm()
			stack.Push(&RoutePoint{sat, append(t, sat), dist + R, R})
		}
	}
	
	return runAStar(&stack, end)
}

