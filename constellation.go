package main

import (
	"github.com/golang/geo/s2"
	"github.com/golang/geo/r3"
	//"fmt"
)

type Satellite struct {
	Name string
	Pos r3.Vector
	VisibleSatellites []*Satellite
	EndPoint bool
	Checked bool
}

type Constellation struct {
	Satellites []Satellite
}

func hasLineOfSight(s1, s2 r3.Vector) bool {
	s1s2 := s2.Sub(s1)
	s1o := s1.Mul(-1)
	s2o := s2.Mul(-1)
	
	if s1o.Dot(s1s2) < 0 {
		return true
	}
	if s2o.Dot(s1s2) > 0 {
		return true
	}
	
    dist := s1.Cross(s2).Norm() / s1.Sub(s2).Norm()
    
    //fmt.Println(" Height from earth surface: ", dist - R)
    
    // Seems that there is either a rounding bug in my or Reaktor code, or then 
    // there is a requirement I was not aware of when doing this challenge and the line
    // of sight is only okay at least 8km above earth surface. Anyway, to counter that I
    // add 8 to earth radius.
    return dist > (R + 8)
}

func latlongToVector(lat, long, h float64) r3.Vector {
    p := s2.PointFromLatLng(s2.LatLngFromDegrees(lat, long))
    
    //fmt.Printf("%v %v %v = %v\n", lat, long, h, p.Mul(R + h))
    return p.Mul(R + h)
}

func (c *Constellation) addSatellite(name string, pos r3.Vector) {
	c.Satellites = append(c.Satellites, 
				Satellite{
					name, 
					pos,
					nil,
					false,
					false})
}

func (c *Constellation) buildNetwork() {
    for i := range c.Satellites[:len(c.Satellites) - 1] {
    	sat1 := &c.Satellites[i]
    	for j := range c.Satellites[i + 1:] {
    		sat2 := &c.Satellites[i + 1 + j]
    		//fmt.Print("Checking ", sat1.Name, " to ", sat2.Name)
    		if hasLineOfSight(sat1.Pos, sat2.Pos) {
    			sat1.VisibleSatellites = append(sat1.VisibleSatellites, sat2)
    			sat2.VisibleSatellites = append(sat2.VisibleSatellites, sat1)
    		}
		}
	}
}



