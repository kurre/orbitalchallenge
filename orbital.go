package main
 
import(
	"fmt"
	"github.com/golang/geo/r3"
	"os"
	"bufio"
	"strings"
	"strconv"
)

const R = 6371.0

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func printDebug(c *Constellation, start, end r3.Vector) {
	for _,sat := range c.Satellites {
		fmt.Printf("%v sees the following satellites: ", sat.Name)
		for _, vs := range sat.VisibleSatellites {
			fmt.Printf("%v ", vs.Name)
		}
		fmt.Println()
	}
	fmt.Printf("The following satellites are visible from start point: ")
	for _,sat := range c.Satellites {
		if hasLineOfSight(start, sat.Pos) {
			fmt.Printf("%v ", sat.Name)
		}
	}
	fmt.Printf("\nThe following satellites are visible from end point: ")
	for _,sat := range c.Satellites {
		if hasLineOfSight(end, sat.Pos) {
			fmt.Printf("%v ", sat.Name)
		}
	}
	fmt.Println()
}

func main() {
	var start, end r3.Vector
	var seed string
	filename := "satellites.txt"
	c := Constellation{nil}
	
 	f, err := os.Open(filename)
    check(err)
    reader := bufio.NewScanner(f)
    for reader.Scan() {
    	parts := strings.Split(reader.Text(), ",")
    	
    	if strings.Contains(parts[0], "SEED") {
    		parts = strings.Split(parts[0], " ")
    		seed = parts[1]
    	} else if strings.Contains(parts[0], "ROUTE") {
			lat1, err := strconv.ParseFloat(parts[1], 64)
			check(err)
			long1, err := strconv.ParseFloat(parts[2], 64)
			check(err)
			lat2, err := strconv.ParseFloat(parts[3], 64)
			check(err)
			long2, err := strconv.ParseFloat(parts[4], 64)
			check(err)
    		
    		start = latlongToVector(lat1, long1, 0)
    		end = latlongToVector(lat2, long2, 0)
    	} else {
			lat, err := strconv.ParseFloat(parts[1], 64)
			check(err)
			long, err := strconv.ParseFloat(parts[2], 64)
			check(err)
			h, err := strconv.ParseFloat(parts[3], 64)
			check(err)
			c.addSatellite(parts[0], latlongToVector(lat, long, h))
		}
    }
    
	c.buildNetwork()
    
    //printDebug(&c, start, end)
    
	route := findRoute(&c, start, end)
	
	fmt.Println("Seed:", seed)
	fmt.Print("Route: ")
	for _,sat := range route {
		fmt.Print(sat.Name + ",")
	}
	fmt.Println()
}    
