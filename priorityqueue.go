package main

import (
	"container/list"
	//"fmt"
	)

type PriorityQueue struct {
	list.List
}

func (pq *PriorityQueue) Push(p *RoutePoint) {
	if pq.Front() == nil {
		//fmt.Println("Adding an item to empty PQ")
		pq.PushFront(p)
		return
	}
	
	added := false
	for e := pq.Front(); e != nil; e = e.Next() {
		if !added && p.expense < e.Value.(*RoutePoint).expense {
			pq.InsertBefore(p, e)
			added = true
		}
		if e.Value.(*RoutePoint).sat.Name == p.sat.Name {
			if added {
				pq.Remove(e)
			}
			return
		}
	}
	if !added {
		pq.PushBack(p)
	}
}

func (pq *PriorityQueue) Pop() *RoutePoint {
	e := pq.Front()
	if e != nil {
		pq.Remove(e)
		return e.Value.(*RoutePoint)
	}
	return nil
}

func (pq *PriorityQueue) Peek() *RoutePoint {
	return pq.Front().Value.(*RoutePoint)
}